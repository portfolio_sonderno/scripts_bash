#!/bin/bash
IFS=$’\n’

md5sum -c --quiet ~/md > ~/error_md.txt | md5sum -c --quiet ~/md 2>> ~/error_md.txt
for error in $(cat ~/error_md.txt)
do
        if [$error = '']; then
                notify-send "Проверка целостности файлов" "Файлы целы";
        else
                notify-send -u critical "Проверка целостности файлов" "$error\nПодробная информация в файле ~/error_md.txt";
        fi
done
