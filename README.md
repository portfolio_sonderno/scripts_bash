# Check md5 sum

You need to create md5sum file:

> $ md5sum /etc/* ~/md

Then you can add this script into crontab to check hashsum of configuration files.
